All movies published to TASVideos are Public Domain works under the CC BY 2.0 license.

Test data | Source
--:|:--
`fm2/4-authors-megamanj.bk2` | [`4131M` NES Mega Man in 09:45.35 by Shinryuu, pirohiko, Maru, and finalfighter](https://tasvideos.org/4131M)
`gbi/g0gotbc-dkkingofswing.bk2` | [`4101M` GBA DK: King of Swing in 17:34.47 by g0goTBC](https://tasvideos.org/4101M)
`lsmv/mtvf1v2-smassmb2.bk2` | [`3644M` SNES Super Mario All-Stars: Super Mario Bros. 2 in 07:16.46 by mtvf1](https://tasvideos.org/3644M)
