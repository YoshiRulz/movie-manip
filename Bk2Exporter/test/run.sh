#!/bin/sh
i=0
# mind the relative directories! bk2export symlink is a sibling of zipbomb dir (the checked-in copy)
xa="../actual_arc"
xe="../expect_arc"
ex="../../../bk2export"
zb="../../../zipbomb/ZipBomb"
single_test() {
	a="../actual.$d"
	printf "converting $f\n"
#	if [ $i -eq 1 ]; then
#		"$ex" "$d" <"$f" >"$a" # haven't actually implemented this yet
#	else
		if [ "$GITLAB_CI" ]; then
			"$ex" "$f" "$a"
		else
			"$ex" "$f" "$a" >/dev/null
		fi
#	fi
	if [ $? -ne 0 ]; then printf "failed\n" && exit 1; fi
	e="$(printf "$f" | sed "s/\\.bk2\$/\\.$d/")"
	printf "checking against $e\n"
	e="../expect/$e"
	if [ "$(od -An -N4 -tx2 "$a")" = " 4b50 0403" ]; then # the GitLab runner doesn't have file or hexdump
		mkdir "$xa" && cd "$xa" && "$zb" "$a" # we need to compare zip contents (because non-determinism), but the GitLab runner also doesn't have bsdtar or unzip, so we have to roll our own...
		mkdir "$xe" && cd "$xe" && "$zb" "$e" # on the bright side, it's guaranteed that BizHawk can process anything that this can because they use the same library
		diff -r "$xe" "$xa" >/dev/null || x=1
		cd "../src"
		rm -r "$xe" "$xa"
	else
		diff "$e" "$a" >/dev/null || x=1
	fi
	rm "$a"
	if [ "$x" ]; then printf "failed\n" && exit 1; fi
#	i=$((1 - i))
}
cd "$(dirname "$0")/data" &&
for d in *; do
	cd "$d/src" &&
	for f in *; do single_test "$d" "$f"; done &&
	cd "../.."
done &&
printf "all passed\n"
