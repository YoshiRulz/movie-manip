#!/bin/sh
set -e
cd "$(dirname "$0")"
dotnet build -c Release -m
rm -fr "../zipbomb"
mv "bin/Release/net6.0" "../zipbomb"
rm -f ../zipbomb/*.dev.json
