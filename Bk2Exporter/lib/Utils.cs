namespace Net.TASBot.Bk2Exporter {
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.IO;
	using System.Linq;
	using System.Reflection;

	using BizHawk.Client.Common;
	using BizHawk.Emulation.Common;

	public static class Utils {
		public static readonly IReadOnlyDictionary<string, Type> ExporterForFileExt;

		static Utils() {
			Dictionary<string, Type> dict = new();
			foreach (var t in typeof(ExporterForAttribute).Assembly.GetTypes()) {
				var attr = t.GetCustomAttributes().OfType<ExporterForAttribute>().FirstOrDefault();
				if (attr != null) dict[attr.FileExtension] = t;
			}
			ExporterForFileExt = dict;
		}

		[Conditional("DEBUG")]
		public static void DebugConsoleWriteLine(string s) => Console.WriteLine(s);

		public static string? HackGetCurrentlyOpenRom(this IEmuClientApi emuClientAPI) {
			var mainform = typeof(EmuClientApi).GetField("_mainForm", BindingFlags.Instance | BindingFlags.NonPublic)!.GetValue(emuClientAPI);
			if (mainform is null) return null;
			return mainform.GetType().GetProperty("CurrentlyOpenRom")!.GetValue(mainform) as string;
		}

		public static IMovie LoadMovieFromFile(string filename) {
			Bk2Movie movie = new(new MockMovieSession(), filename);
			movie.Load(false);
			return movie;
		}

		private sealed class MockMovieSession : IMovieSession {
			public string BackupDirectory {
				get => throw new NotImplementedException();
				set => throw new NotImplementedException();
			}

			public IMovie Movie => throw new NotImplementedException();

			public IMovieController MovieController => throw new NotImplementedException();

			public IInputAdapter MovieIn {
				set => throw new NotImplementedException();
			}

			public IInputAdapter MovieOut => throw new NotImplementedException();

			public bool NewMovieQueued => throw new NotImplementedException();

			public string QueuedSyncSettings => throw new NotImplementedException();

			public bool ReadOnly {
				get => throw new NotImplementedException();
				set => throw new NotImplementedException();
			}

			public IMovieConfig Settings { get; } = new MovieConfig();

			public IStickyAdapter StickySource {
				get => throw new NotImplementedException();
				set => throw new NotImplementedException();
			}

			public bool CheckSavestateTimeline(TextReader? reader) => throw new NotImplementedException();

			public void ConvertToTasProj() => throw new NotImplementedException();

			public IMovieController GenerateMovieController(ControllerDefinition? definition) => throw new NotImplementedException();

			public IMovie Get(string? path) => throw new NotImplementedException();

			public void HandleFrameAfter() => throw new NotImplementedException();

			public void HandleFrameBefore() => throw new NotImplementedException();

			public bool HandleLoadState(TextReader? reader) => throw new NotImplementedException();

			public void HandleSaveState(TextWriter? writer) => throw new NotImplementedException();

			public void PopupMessage(string? message) => throw new NotImplementedException();

			public string QueuedCoreName => throw new NotImplementedException();

			public void QueueNewMovie(IMovie? movie, bool record, string? systemId, IDictionary<string, string>? preferredCores) => throw new NotImplementedException();

			public void RunQueuedMovie(bool recordMode, IEmulator? emulator) => throw new NotImplementedException();

			public void StopMovie(bool saveChanges) => throw new NotImplementedException();

			public IDictionary<string, object> UserBag {
				get => throw new NotImplementedException();
				set => throw new NotImplementedException();
			}
		}
	}
}
