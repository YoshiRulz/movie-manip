namespace Net.TASBot.Bk2Exporter {
	using System;
	using System.Collections.Generic;
	using System.IO;

	using BizHawk.Client.Common;

	/// <remarks>implementation based on a pastebin maintained by TiKevin83: https://pastebin.com/6jaPXm4H</remarks>
	[ExporterFor("gbi")]
	public sealed class GbiExporter : IMovieExporter {
		private const int CYCLES_PER_FRAME = 280896;

		private const int MNEMONIC_SUBSET_LEN = 10;

		/// <remarks>.bk2 <c>UDLRSsBAlr</c> to .gbi <c>rlDULRSsBA</c></remarks>
		private static readonly short[] MAPPING = { 0x0040, 0x0080, 0x0020, 0x0010, 0x0008, 0x0004, 0x0002, 0x0001, 0x0100, 0x0200 };

		public bool Export(IMovie movie, Stream outputStream, out IReadOnlyList<string> errors, out IReadOnlyList<string> warnings) {
			warnings = new List<string> { "axes and the power button are not representable in this format" };
#pragma warning disable CA2000 // see finally block
			StreamWriter outputSW = new(outputStream);
#pragma warning restore CA2000
			try {
				ulong cycle = 0;
				var lastMnemonic = string.Empty;
				var totalFrames = movie.InputLogLength;
				for (var frame = 0; frame < totalFrames; frame++) {
					cycle += CYCLES_PER_FRAME;
					var frameMnemonic = movie.GetInputLogEntry(frame).Substring(25, MNEMONIC_SUBSET_LEN);
					if (frameMnemonic == lastMnemonic) continue;
					lastMnemonic = frameMnemonic;
					var o = 0;
					for (var i = 0; i < MNEMONIC_SUBSET_LEN; i++) o ^= frameMnemonic[i] == '.' ? 0 : MAPPING[i];
					outputSW.WriteLine($"{(cycle - 83776 - 4096) / 4096:X8} {o:X4}");
				}
				errors = new List<string>();
				return true;
#pragma warning disable CA1031
			} catch (Exception e) {
#pragma warning restore CA1031
				errors = new List<string> { $"{e.Message}\n{e.StackTrace}" };
				return false;
#if NET6_0
			} finally {
				outputSW.Close(); // under net48, this closes the wrapped stream, causing the file write to fail
#endif
			}
		}
	}
}
