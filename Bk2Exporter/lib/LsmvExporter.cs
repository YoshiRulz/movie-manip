namespace Net.TASBot.Bk2Exporter {
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.Linq;
	using System.Text;

	using BizHawk.Client.Common;
	using BizHawk.Emulation.Cores.Nintendo.SNES;

	using static BizHawk.Emulation.Cores.Nintendo.SNES.LibsnesControllerDeck;

	/// <remarks>
	/// https://tasvideos.org/Lsnes/Movieformat<br/>
	/// implementation based on https://github.com/TheMas3212/bizhawk-to-lsnes at <c>554ee520d</c>
	/// </remarks>
	[ExporterFor("lsmv")]
	public sealed class LsmvExporter : ZipCompressedMovieExporterBase {
		protected override bool PrepareExport(IMovie movie, out IReadOnlyDictionary<string, byte[]> filenameContentsMap, out IReadOnlyList<string> errors, out IReadOnlyList<string> warnings) {
			List<string> warningList = new();
			try {
				if (movie.SystemID != "SNES") throw new Exception("cannot serialise movie as .lsmv when system is not SNES");
				if (!(ConfigService.LoadWithType(movie.SyncSettingsJson) is LibsnesCore.SnesSyncSettings syncSettings)) throw new Exception("failed to read SNES sync settings from movie; check it was recorded on BSNES");
				if (syncSettings.LeftPort == ControllerType.Unplugged && syncSettings.RightPort == ControllerType.Unplugged) warningList.Add("no virtual controllers plugged in, converting only resets");

				// Set basic settings
#pragma warning disable CA1308
				static string SerialiseSNESControllerType(ControllerType type) => (type == ControllerType.Unplugged ? null : Enum.GetName(typeof(ControllerType), type)?.ToLowerInvariant()) ?? "none";
#pragma warning restore CA1308
				static byte[] UnicodeBytes(string s) => Encoding.UTF8.GetBytes(s);
				static byte[] MD5(string s) {
#pragma warning disable CA5351
					using var md5 = System.Security.Cryptography.MD5.Create();
#pragma warning restore CA5351
					return md5.ComputeHash(UnicodeBytes(s));
				}
				var totalFrames = movie.InputLogLength;
				Dictionary<string, byte[]> lsmvDict = new() {
					["controlsversion"] = UnicodeBytes("0"),
					["coreversion"] = UnicodeBytes("bsnes v085 (Compatibility core)"),
					["gametype"] = UnicodeBytes("snes_ntsc"),
					["systemid"] = UnicodeBytes("lsnes-rr1"),
					["setting.hardreset"] = UnicodeBytes("1"),
					["authors"] = UnicodeBytes(movie.Author),
					["rom.hint"] = UnicodeBytes(movie.GameName),
					["projectid"] = UnicodeBytes(string.Concat(
						MD5(string.Concat(movie.Author, movie.GameName, totalFrames.ToString(CultureInfo.InvariantCulture), movie.GetInputLogEntry(totalFrames / 2)))
							.Select(b => b.ToString("X2", CultureInfo.InvariantCulture)))), // Python was `lsmv_dict['projectid'] = hashlib.md5(str(header_dict)).hexdigest()`
					["port1"] = UnicodeBytes(SerialiseSNESControllerType(syncSettings.LeftPort)),
					["port2"] = UnicodeBytes(SerialiseSNESControllerType(syncSettings.RightPort)),
				};

				// Generate rrdata
				static void SerialiseRerecordCount(Action<byte> appendChar, ulong rrcount) {
					if (rrcount == 0) return;
					if (rrcount > 0xFFFFFF) {
						appendChar(0x7F);
						appendChar(0x00);
						appendChar(0xFE);
						appendChar(0xFE);
						appendChar(0xFE);
						// no recursion
					} else if (rrcount > 0xFFFFF) {
						appendChar(0x7F);
						appendChar(0x00);
						appendChar(0x0E);
						appendChar(0xFE);
						appendChar(0xFE);
						SerialiseRerecordCount(appendChar, rrcount - 0x100000);
					} else if (rrcount > 0xFFFF) {
						appendChar(0x5F);
						appendChar(0x00);
						appendChar(0xFE);
						appendChar(0xFE);
						SerialiseRerecordCount(appendChar, rrcount - 0x10000);
					} else if (rrcount > 0xFFF) {
						appendChar(0x5F);
						appendChar(0x00);
						appendChar(0x0E);
						appendChar(0xFE);
						SerialiseRerecordCount(appendChar, rrcount - 0x1000);
					} else if (rrcount > 0xFF) {
						appendChar(0x3F);
						appendChar(0x00);
						appendChar(0xFE);
						SerialiseRerecordCount(appendChar, rrcount - 0x100);
					} else if (rrcount > 1) {
						appendChar(0x3F);
						appendChar(0x00);
						appendChar(unchecked((byte) (rrcount - 2)));
					} else {
						// rrcount == 1
						appendChar(0x1F);
						appendChar(0x00);
					}
				}
				var rrcount = movie.Rerecords + 1;
				lsmvDict["rerecords"] = UnicodeBytes(rrcount.ToString(CultureInfo.InvariantCulture));
				List<byte> rrlist = new();
				SerialiseRerecordCount(rrlist.Add, rrcount);
				lsmvDict["rrdata"] = rrlist.ToArray();

				// Input Conversion
				static IDictionary<string, IList<bool>> InitInputDict() => new Dictionary<string, IList<bool>> {
					["A"] = new List<bool>(),
					["B"] = new List<bool>(),
					["X"] = new List<bool>(),
					["Y"] = new List<bool>(),
					["u"] = new List<bool>(),
					["d"] = new List<bool>(),
					["l"] = new List<bool>(),
					["r"] = new List<bool>(),
					["s"] = new List<bool>(),
					["S"] = new List<bool>(),
					["L"] = new List<bool>(),
					["R"] = new List<bool>(),
				};
				Dictionary<string, IList<bool>> inputDataSystem = new();
				Dictionary<string, IDictionary<string, IList<bool>>> inputDataPort1 = new();
				Dictionary<string, IDictionary<string, IList<bool>>> inputDataPort2 = new();
#pragma warning disable SA1413
				IReadOnlyList<string> conmapPort1 = syncSettings.LeftPort switch {
					ControllerType.Gamepad => new[] { "P1" },
					ControllerType.Multitap => new[] { "P1", "P2", "P3", "P4" },
					_ => Array.Empty<string>()
				};
				IReadOnlyList<string> conmapPort2 = syncSettings.RightPort switch {
					ControllerType.Gamepad => new[] { "P5" },
					ControllerType.Multitap => new[] { "P5", "P6", "P7", "P8" },
					_ => Array.Empty<string>()
				};
#pragma warning restore SA1413
				inputDataSystem["SoftReset"] = new List<bool>();
				inputDataSystem["HardReset"] = new List<bool>();
				foreach (var player in conmapPort1) inputDataPort1[player] = InitInputDict();
				foreach (var player in conmapPort2) inputDataPort2[player] = InitInputDict();

				for (var frame = 0; frame < totalFrames; frame++) {
					var frameFields = movie.GetInputLogEntry(frame).Substring(1).Split('|');
					var frameFieldCount = frameFields.Length - 1;
					inputDataSystem["SoftReset"].Insert(frame, frameFields[0][0] == 'r');
					inputDataSystem["HardReset"].Insert(frame, frameFields[0][1] == 'P');
					for (var pNum = 1; pNum < frameFieldCount; pNum++) {
						var (inputData, player) = pNum - 1 < conmapPort1.Count ? (inputDataPort1, conmapPort1[pNum - 1]) : (inputDataPort2, conmapPort2[pNum - 1 - conmapPort1.Count]);
						inputData[player]["u"].Insert(frame, frameFields[pNum][0] == 'U');
						inputData[player]["d"].Insert(frame, frameFields[pNum][1] == 'D');
						inputData[player]["l"].Insert(frame, frameFields[pNum][2] == 'L');
						inputData[player]["r"].Insert(frame, frameFields[pNum][3] == 'R');
						inputData[player]["A"].Insert(frame, frameFields[pNum][9] == 'A');
						inputData[player]["B"].Insert(frame, frameFields[pNum][7] == 'B');
						inputData[player]["X"].Insert(frame, frameFields[pNum][8] == 'X');
						inputData[player]["Y"].Insert(frame, frameFields[pNum][6] == 'Y');
						inputData[player]["s"].Insert(frame, frameFields[pNum][4] == 's');
						inputData[player]["S"].Insert(frame, frameFields[pNum][5] == 'S');
						inputData[player]["L"].Insert(frame, frameFields[pNum][10] == 'l');
						inputData[player]["R"].Insert(frame, frameFields[pNum][11] == 'r');
					}
				}

				// Generate lsnes Inputfile
				static string SerialiseFieldForLSMVFrame(IDictionary<string, IList<bool>> playerData, int frameNum1) => string.Concat(
					playerData["B"][frameNum1] ? 'B' : '.',
					playerData["Y"][frameNum1] ? 'Y' : '.',
					playerData["s"][frameNum1] ? 's' : '.',
					playerData["S"][frameNum1] ? 'S' : '.',
					playerData["u"][frameNum1] ? 'u' : '.',
					playerData["d"][frameNum1] ? 'd' : '.',
					playerData["l"][frameNum1] ? 'l' : '.',
					playerData["r"][frameNum1] ? 'r' : '.',
					playerData["A"][frameNum1] ? 'A' : '.',
					playerData["X"][frameNum1] ? 'X' : '.',
					playerData["L"][frameNum1] ? 'L' : '.',
					playerData["R"][frameNum1] ? 'R' : '.');
				var recordFields = new string[1 + conmapPort1.Count + conmapPort2.Count];
				StringBuilder sbRecords = new();
				for (var recordNum = 0; recordNum < totalFrames; recordNum++) {
					recordFields[0] = $"F{(inputDataSystem["SoftReset"][recordNum] ? 'R' : '.')}{(inputDataSystem["HardReset"][recordNum] ? 'H' : '.')}";
					for (var i = 0; i < conmapPort1.Count; i++) recordFields[1 + i] = SerialiseFieldForLSMVFrame(inputDataPort1[conmapPort1[i]], recordNum);
					for (var i = 0; i < conmapPort2.Count; i++) recordFields[1 + conmapPort1.Count + i] = SerialiseFieldForLSMVFrame(inputDataPort2[conmapPort2[i]], recordNum);
					var record = string.Join("|", recordFields);
					Utils.DebugConsoleWriteLine(record);
					sbRecords.AppendLine(record);
				}
				lsmvDict["input"] = UnicodeBytes(sbRecords.ToString());

				filenameContentsMap = lsmvDict;
				errors = new List<string>();
				warnings = warningList;
				return true;
#pragma warning disable CA1031
			} catch (Exception e) {
#pragma warning restore CA1031
				filenameContentsMap = new Dictionary<string, byte[]>();
				errors = new List<string> { $"{e.Message}\n{e.StackTrace}" };
				warnings = warningList;
				return false;
			}
		}
	}
}
