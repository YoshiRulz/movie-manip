namespace Net.TASBot.Bk2Exporter {
	using System;

	[AttributeUsage(AttributeTargets.Class)]
	internal sealed class ExporterForAttribute : Attribute {
		public readonly string FileExtension;

		public ExporterForAttribute(string fileExt) => FileExtension = fileExt;
	}
}
