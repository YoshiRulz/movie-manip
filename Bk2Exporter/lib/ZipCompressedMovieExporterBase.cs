namespace Net.TASBot.Bk2Exporter {
	using System.Collections.Generic;
	using System.IO;

	using BizHawk.Client.Common;
	using SharpCompress.Archives.Zip;
	using SharpCompress.Common;
	using SharpCompress.Writers;

	public abstract class ZipCompressedMovieExporterBase : IMovieExporter {
		public bool Export(IMovie movie, Stream outputStream, out IReadOnlyList<string> errors, out IReadOnlyList<string> warnings) {
			if (!PrepareExport(movie, out var filenameContentsMap, out errors, out warnings)) return false;
			using var archive = ZipArchive.Create();
#pragma warning disable CA2000
			foreach (var entry in filenameContentsMap) archive.AddEntry(entry.Key, new MemoryStream(entry.Value), true, entry.Value.Length);
#pragma warning restore CA2000
			archive.SaveTo(outputStream, new(CompressionType.Deflate) { LeaveStreamOpen = true });
			return true;
		}

		/// <param name="filenameContentsMap">keys are filenames relative to output archive; values are file contents, which will be encoded with the default UTF-8 string serializer</param>
		protected abstract bool PrepareExport(IMovie movie, out IReadOnlyDictionary<string, byte[]> filenameContentsMap, out IReadOnlyList<string> errors, out IReadOnlyList<string> warnings);
	}
}
