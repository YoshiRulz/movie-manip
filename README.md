# Overview

All code in this repo is licensed under the GPL, making it free for everyone, forever. The full text is in [LICENSE.txt](/LICENSE.txt).
The license does not apply to consumed libraries or to code in Git submodules. The license also does not apply to the "movie" files used as test data, which come from TASVideos and are thus Public Domain works under CC BY 2.0 (full credits [here](/Bk2Exporter/test/movie_credits.md)).

Jump to:
* [Bk2Exporter](#bk2exporter)
	* [Usage](#usage)
	* [Building](#building)
* [Contributing](#contributing)

# Bk2Exporter

This project aims to provide `.bk2 -> .*` *exporting* to complement BizHawk's `.* -> .bk2` *importing*. It replaces the dissociated scripts that were previously in use by the community.

## Usage

Conversion can be done from within BizHawk via an external tool, or from the command-line.

### CLI

> Note: The packaging of binaries is not currently automated. You'll need to clone the repo, build, and link/alias `bk2export` to `<repo>/Bk2Exporter/cli/bin/Release/net6.0/Bk2ExporterCLI`.  
> On Windows, the executable would be `Bk2ExporterCLI.exe`, or you could use `Bk2ExporterCLI.dll` on any platform if you run as `dotnet Bk2ExporterCLI.dll`.

You'll need the .NET 6 Runtime (or newer) installed to run the CLI (Linux: package is usually `dotnet-runtime-6.0`, [full instructions here](https://docs.microsoft.com/en-gb/dotnet/core/install/linux); Windows: comes with Windows 10 IIRC, but can be [downloaded separately](https://dotnet.microsoft.com/download/dotnet/6.0), full instructions on [MSDN](https://docs.microsoft.com/en-gb/dotnet/core/install/windows#download-and-manually-install)).

Running the program without arguments prints its intended usage:
```
$> ./bk2export
usage:
	with filenames: `./bk2export from.bk2 to.lsmv` or `./bk2export from.bk2 lsmv`
```

### External tool

> Note: This isn't well-tested, but it shares most of its code with the CLI so hopefully it's okay.

You'll need BizHawk 2.8.
Close EmuHawk and put `Bk2ExporterTool.dll` into `<BizHawk>/ExternalTools`.
Open EmuHawk, load a rom and movie, and select `Tools` > `External Tool` > `Bk2Exporter`. In the dialog, select a file extension from the dropdown and click `=>`.
The converted movie will be placed in `<BizHawk>/Movies` (configurable in `Config` > `Paths...` > `Global` > `Movies`).

## Building

You'll need the .NET 6 SDK (or newer) installed to build anything (Linux: package is usually `dotnet-sdk-6.0`, [full instructions here](https://docs.microsoft.com/en-gb/dotnet/core/install/linux); Windows: comes with VS2019, or can be [downloaded separately](https://dotnet.microsoft.com/download/dotnet/6.0), full instructions on [MSDN](https://docs.microsoft.com/en-gb/dotnet/core/install/windows#download-and-manually-install)).

Run either `build_cli_release.sh` for the CLI or `build_exttool_release.sh` for the ext. tool. PowerShell scripts for building on Windows aren't currently available, but you can open the relevant `.csproj` in Visual Studio to build and debug, or use the BASH scripts from within WSL2 if it has a copy of the SDK.

The build output is placed in the conventional location (`Bk2Exporter/<project>/bin/<Debug or Release>/<.NET target>`, with the executable named the same as the `.csproj` e.g. `Bk2Exporter/cli/bin/Release/net6.0/Bk2ExporterCLI.dll`).

When building the ext. tool, setting the environment variable `BIZHAWK_HOME` to BizHawk's install location will put a copy into its `ExternalTools` dir for you.

# Contributing

Bug reports and Merge Requests are welcome. MRs should follow the project style of tab-indentation and Java-style (K&R) braces.

The maintainers can also be reached in the [TASBot Discord](https://discord.tas.bot), specifically the `#tasbot-dev` channel (ping `YoshiRulz#4472`).
