#!/bin/sh
set -e
cd "$(dirname "$0")"
git submodule update
cd "submodule"
./Dist/BuildRelease.sh "$@"
git restore "global.json"
../update_assembly_refs.sh
